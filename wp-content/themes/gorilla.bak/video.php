<?php
/**
 * Template Name: Video
 */
// Get all ancestors, then get the first in the array (parent)
$parentArray = get_post_ancestors($post->ID);
$pageParent = $parentArray[0];
$videoPages = get_posts('numberposts=-1&orderby=menu_order&order=ASC&post_type=page&post_status=publish&post_parent='.$pageParent);
get_header(); ?>

<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		
	<div id="mainContent" class="videoPanel">
		<?php //the_content('Read the rest of this entry &raquo;'); ?>
		<?php 
			$embedcode = get('video_embed_code'); 
			if (!empty($embedcode)) {
				echo('<div class="videoplayer">');
				echo get('video_embed_code'); 
				echo('</div>');
			}
		?>
		
		<?php include('inc_videonav.php');?>
	</div>

	<div id="sidebox" class="videoPanelSideBox">
		<h2 class="h1"><?php echo(get_the_title($post->post_parent)); ?></h2>
		<h1 class="h1"><?php the_title(); ?></h1>
		<?php if (get('video_sub_title')) {
			echo('<strong>'.get('video_sub_title').'</strong>');
		}?>
	
		<dl>
			<?php if (get('video_director')) {
				echo('<dt>Director</dt>');
				echo('<dd>'.get('video_director').'</dd>');
			}?>
			
			<?php if (get('video_prodco')) {
				echo('<dt>Prod co.</dt>');
				echo('<dd>'.get('video_prodco').'</dd>');
			}?>
			
			<?php if (get('video_agency')) {
				echo('<dt>Agency</dt>');
				echo('<dd>'.get('video_agency').'</dd>');
			}?>
		</dl>
		
		<?php
		$embedcode_big = get('video_embed_code_big');
		if (!empty($embedcode_big)) {
			echo('<div class="clear"></div><a id="videolightbox" class="h1" href="');
			echo get('video_embed_code_big'); 
			echo('">Play full screen</a>');
		}
		?>
		<div id="spread">
			<a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet this video</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
		
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=208915032495996&amp;href&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:50px; height:20px; " allowTransparency="true"></iframe>
		</div>
	</div>

	<?php endwhile; ?>
<?php else : ?>
	<h1>Not Found</h2>
	<p>Sorry, but you are looking for something that isn't here.</p>
	<?php get_search_form(); ?>
<?php endif; ?>

<?php get_footer(); ?>