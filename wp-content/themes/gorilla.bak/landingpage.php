<?php
/**
 * Template Name: Landing page
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta property="og:image" content="<?php bloginfo('stylesheet_directory'); ?>/images/icon.png" />
<meta http-equiv="refresh" content="3; url=/home">
<title><?php bloginfo('name'); ?><?php wp_title(' |', true, 'left'); ?></title>
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.gif" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/global.css" type="text/css" media="screen" />
</head>
<body <?php body_class($class); ?>>
<div id="mainContent">
	<a href="home" title="Go to the home page"><img src="wp-content/themes/gorilla/images/landing-logo.jpg" alt="Gorilla Logo" width="952" height="360"></a>
</div>
</body>
</html>