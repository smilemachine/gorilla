<?php
get_header(); ?>

<div id="mainContent" class="contentArea postArea">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<div class="post">
				<h1><a href="<?php echo(get_permalink( $id )); ?>"><?php the_title(); ?></a></h1>
				<small><?php echo get_the_date(); ?>&nbsp;&nbsp;|&nbsp;&nbsp;<?php the_category(', '); ?></small>
				<?php the_content('Read the rest of this entry &raquo;'); ?>
			</div>
		<?php endwhile; ?>
	<?php else : ?>
		<h1>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>
	<?php endif; ?>
	
	<div class="nav h1 nav-previous"><?php next_posts_link('Older Entries') ?></div>
	<div class="nav h1 nav-next"><?php previous_posts_link('Newer Entries') ?></div>
</div>

<div id="sidebox">
	<h2 class="h1">Latest tweets</h2>
	<div class="tweet"></div> 
	<div class="social">FOLLOW US<br></div>
	<div class="socialicons"><a href="http://twitter.com/#!/gorilla_editors" title="gorilla_editors on twitter" target="_blank"><img src="/wp-content/themes/gorilla/images/twitter.png"></a>&nbsp;&nbsp;<a href="https://www.facebook.com/pages/Gorilla-Film-Editors/200961099944016" title="Gorilla on Facebook" target="_blank"><img src="/wp-content/themes/gorilla/images/facebook.png"></a></div> 
</div>

<?php get_footer(); ?>
