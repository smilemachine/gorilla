<nav id="videoNav">
	<?php
	// Create arrays
	$categoryArray = array();
	//$videoArray = array();
	
	// Loop through all child pages
	foreach($videoPages as $page) {
		// Store the ID of current page in the loop
		$currentPageID = $page->ID;
		
		// Find out what category the video is in
		$currentCategory = get('video_category',1,1,true,$currentPageID);
		$isHidden = get('video_hidden',1,1,true,$currentPageID);
		if (empty($isHidden)) { 
			if (!in_array($currentCategory, $categoryArray)) { 
				$categoryArray[] = $currentCategory;
			}
		}
		
	}
	
	// Begin outputting
	if (!empty($videoPages)) {
		// List Category navigation
		echo('<ul class="categories">');
		foreach($categoryArray as $category) {
			$categoryPath = strtolower($category);
			echo('<li><a href="#'.slug($category).'">'.$category.'</a></li>');
		}
		echo('</ul>');
		
		// List thumbnails
		
		foreach($categoryArray as $category) {
			$categoryPath = strtolower($category);
			echo('<div id="'.slug($category).'" class="thumbs">');
			
			//Loop through all child pages and extract the category, if it matches the one we're checking, output its data
			
			foreach($videoPages as $page) {
				$currentCategory = get('video_category',1,1,true,$page->ID);
				if ($currentCategory == $category) {
					echo('<a href="'.get_permalink($page->ID).'#'.slug($category).'"');
					if($page->ID == $post->ID) { echo(' class="active"'); }
					echo('>');
					if (get_the_post_thumbnail($page->ID)) { 
						echo get_the_post_thumbnail($page->ID, array(61,44), array('title'=> ''));
					} else {
						echo('<img src="');
						bloginfo('stylesheet_directory');
						echo ('/images/no-thumb.gif" height="44" width="60" alt="No thumbnail">');
					}
					
					echo('<span class="caption">'.$page->post_title);
					if(get('video_director',1,1,true,$page->ID)) { echo('&nbsp;&nbsp; <strong>Director:</strong> '.get('video_director',1,1,true,$page->ID).' </span>'); }
					echo('</a>');
				}
			}
			echo('</div>');
		}
	}
	?>
</nav>