
$(document).ready(function(){
	
	$('#scroller').jScrollPane();
	
	$(".tweet").tweet({
		username: "gorilla_editors",
		join_text: "auto",
		avatar_size: 0,
		count: 2,
		auto_join_text_default: "",
		auto_join_text_ed: "",
		auto_join_text_ing: "we",
		auto_join_text_reply: "",
		auto_join_text_url: "",
		loading_text: "loading tweets..."
	});
	
	// Slideshows
	$(function(){
		$('.slideshow').slides({
			preload: true,
			container: 'slide-container',
			preloadImage: '../images/loading.gif',
			generateNextPrev: false,
			pagination: false,
			generatePagination: false,
			effect: 'fade',
			crossfade: true,
			play: 4000,
			fadeSpeed: 1000,
			pause: 3000,
			hoverPause: false
		});
	});
	
	$('a#videolightbox').fancybox({
		'overlayOpacity'	: '0.9',
		'overlayColor'		: '#000',
		'autoScale'			: true,
		'transitionIn'		: 'fade',
		'transitionOut'	: 'none',
		'padding'			: '0',
		'width'				: 900, 
		'height'				: 505,
		'type'				: 'iframe'
	});
	
	$('#videoNav').tabs();
	$('#videoNav ul li a').click(function() {
		location.hash = $(this).attr('href');
	});
	
	// Editors drop down in JS
	$('#mainNav .page-item-9 > a').attr('href','#');
	$('#mainNav .page-item-9 > a').click(function(e) {
		e.preventDefault();
		if (!$(this).hasClass('current_page_parent')) {
			$(this).siblings('ul').slideToggle('fast');
		}
		
	});
	
});