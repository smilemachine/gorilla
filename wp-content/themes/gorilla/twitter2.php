<?php
require_once("twitteroauth/twitteroauth/twitteroauth.php"); //Path to twitteroauth library

	class Twitter{


		public $twitteruser = "gorilla_editors";
		public $notweets = 2;
		public $consumerkey = "u4dPkET4JsTI0p7b1a7e5g";
		public $consumersecret = "PoBO3xCDJp9NcyY3OU5vIcX3ZsnPxadAVreS4L3MCU";

		public $accesstoken = "252103510-1HnKgtWzlF6X336XVB2eeSojb9w1XKktO3GCusxo";
		public $accesstokensecret = "RDR8cmDMaHQQyhqpp2I0iF4QLLphmsjkoxFSC87UKk";



		public function getConnectionWithAccessToken($cons_key, $cons_secret, $oauth_token, $oauth_token_secret) {
	  		$connection = new TwitterOAuth($cons_key, $cons_secret, $oauth_token, $oauth_token_secret);
		  	return $connection;
		}

		  
		public function getTweets(){

			$connection = $this->getConnectionWithAccessToken($this->consumerkey, $this->consumersecret, $this->accesstoken, $this->accesstokensecret);
			$tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=".$this->twitteruser."&count=".$this->notweets."&exclude_replies=true");
			return $tweets;
		} 


		public function formatTweet($tweet){

			$text = $tweet->text;

			// if tweet is reteeted set the retweet to tweet
			if(!empty($tweet->retweeted_status)){
				
				$retweet = $tweet->retweeted_status;	
				$tweet = $retweet;
				$text = $tweet->text;			
			}

			// if there are urls in tweet add them as links
			if(!empty($tweet->entities->urls)){

				$urls = $tweet->entities->urls;	
				$text = $this->addUrls($urls, $text);		
			}

			// if there are media urls in tweet add them as links
			if(!empty($tweet->entities->media)){

				$urls = $tweet->entities->media;	
				$text = $this->addUrls($urls, $text);		
			}

			// if there are any user_mentions add them as links
			if(!empty($tweet->entities->user_mentions)){								

				$user_mentions = $tweet->entities->user_mentions;
				$text = $this->addUserMentions($user_mentions, $text);								
			}

			return $text;
		}


		public function addUrls($urls, $tweet){

			foreach($urls as $url){

				$shortUrl = $url->display_url;
				$fullUrl = $url->url;								
				$link = '<a target="_blank" href="' . $fullUrl . '">' . $shortUrl . '</a>';
				$tweet = str_replace($fullUrl, $link, $tweet);	
			}

			return $tweet;
		}


		public function addUserMentions($user_mentions, $tweet){

			foreach($user_mentions as $user_mention){

				$screen_name = '@' . $user_mention->screen_name;
				$link = '<a target="_blank" href="https://twitter.com/' . $user_mention->screen_name . '">' . $screen_name . '</a>';
		 		$tweet = str_replace($screen_name, $link, $tweet);
			}

		 	return $tweet;
		}
	}
	

	$twitterArray = new Twitter();
	$tweets = $twitterArray->getTweets();
	$i = 0;
	foreach ($tweets as $tweet) {
		if($i !== 0){
			echo '<br />';	
		}
		$tweet = $twitterArray->formatTweet($tweet);
		echo $tweet;
		$i++;
	}
?>