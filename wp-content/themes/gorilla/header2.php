<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta property="og:image" content="<?php bloginfo('stylesheet_directory'); ?>/images/icon.png" />
<title><?php bloginfo('name'); ?><?php wp_title(' |', true, 'left'); ?></title>

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/images/favicon.gif" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald" type="text/css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.fancybox-1.3.4.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/jquery.jscrollpane.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/global.css" type="text/css" media="screen" />
<!--[if IE 7]><link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/ie8.css" type="text/css" /><![endif]-->

<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.tweet.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/slides.min.jquery.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.fancybox-1.3.4.pack.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.mousewheel.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.jscrollpane.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/global.js"></script>


<?php wp_head(); ?>
</head>
<body <?php body_class($class); ?>>
<div id="wrapper">
	<div id="container">
		<header>
			<a href="/home" id="mainLogo" title="Back to home"><?php bloginfo('name'); ?> Home</a>
			<span id="collective">Film Editing Collective</span>
		</header>
		<nav id="mainNav">
			<ul>
				<?php
					$excludedPages = '4,159';
					$allEditors = get_posts(array('post_type' => 'page', 'numberposts' => -1 ));
					
					foreach ($allEditors as $editor) {
						if ( get('hidden', 1, 1, true, $editor->ID) ) {
							$excludedPages .= ','.$editor->ID;
						}
					}
					echo '<!--'.$excludedPages.'-->';

					//wp_list_pages('title_li=&sort_order=desc&exclude=4,159&depth=2');
					wp_list_pages(array(
						'title_li' => '',
						'sort_order' => 'desc',
						'exclude' => $excludedPages,
						'depth' => '2'
					)); 
				?> 
			</ul>
		</nav>