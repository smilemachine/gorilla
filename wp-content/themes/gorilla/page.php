<?php
get_header(); ?>

<div id="mainContent" class="contentArea">
	<div id="scroller" class="scroll-pane">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			
					
					<?php the_content('Read the rest of this entry &raquo;'); ?>
			
			<?php endwhile; ?>
		<?php else : ?>
			<h1>Not Found</h2>
			<p>Sorry, but you are looking for something that isn't here.</p>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</div>



<?php get_footer(); ?>
