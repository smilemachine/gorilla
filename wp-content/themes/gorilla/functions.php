<?php

add_theme_support( 'post-thumbnails', array( 'page' ) );
set_post_thumbnail_size(640, 360, true);


function getGenericMagicFieldObjects($fieldNames) 
	{
		  // Build an array of the bottom links to be shown
		  // Set an array of the fields we're looking for
		  $fieldArray = $fieldNames;
		  $numberOfItems = getGroupDuplicates($fieldArray[0]);
		  $groupOrder = getGroupOrder($fieldArray[0]);	  
	 
		  $linkArray = array();
	 
		  // Loop through the number of items and get the link information
		  if (is_array($groupOrder))
		  {
				foreach($groupOrder as $order => $itemId)
				{
					 $newLinkItem = array();
					 $currentLinkHasContent = false;
					 
					 // Now loop through the different fields and get them
					 foreach($fieldArray as $fieldName)
					 {
						  $fieldValue = get($fieldName, $itemId);
						  
						  $newLinkItem[$fieldName] = $fieldValue;
						  
						  if (trim($fieldValue) != '')
						  {
								$currentLinkHasContent = true;
						  }
					 }
					 
					 // It's possible that there's an empty link item on this page.  Only add it if
					 //  some of the fields have content
					 if ($currentLinkHasContent)
					 {
						  $linkArray[] = $newLinkItem;
					 }
				}
		  }
		  
		  return $linkArray;
	}


function slug($str) {
	$str = strtolower(trim($str));
	$str = preg_replace('/[^a-z0-9-]/', '-', $str);
	$str = preg_replace('/-+/', "-", $str);
	return $str;
}

?>
