<?php
/**
 * Template Name: Homepage
 */

$videoPages = get_posts('numberposts=-1&orderby=rand&order=desc&post_type=page&post_status=publish');
get_header(); ?>

<div id="mainContent">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content('Read the rest of this entry &raquo;'); ?>
			
			<div class="slideshow">
				<div class="slide-container">
					<?php
						foreach($videoPages as $page) {
							$currentlyChecking = get('video_category',1,1,true,$page->ID);
							if ($currentlyChecking) {
								if (get_the_post_thumbnail($page->ID)) {
									//$editor = get_the_title($page->post_parent);
									
									$editor = $page->post_parent;
									
									// if ($editor == 'Matt Chodan') {
									// 	$editor = 'matt-chodan';
									// } else if ($editor == 'Max Young') {
									// 	$editor = 'max-young';
									// } else if ($editor == 'Joe Parsons') {
									// 	$editor = 'joe-parsons';
									// }
									
									
									// echo('<div><a href="/editors/'.slug(get_the_title($editor)).'/'.$page->post_name.'/#'.slug(get('video_category',1,1,true,$page->ID)).'">');
									echo('<div><a href="'.get_permalink($page->ID).'#'.slug(get('video_category',1,1,true,$page->ID)).'">');
									
									echo get_the_post_thumbnail($page->ID, array(640,360), array('title'=> 'View: '.$page->post_title));
									echo('</a></div>');
								}
							}
							
							$currentlyChecking = null;
						}
					?>
				</div>
			</div>
			
		<?php endwhile; ?>
	<?php else : ?>
		<h1>Not Found</h2>
		<p>Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>
	<?php endif; ?>
</div>

<div id="sidebox">
	<h2 class="h1">Latest tweets</h2>
	<div class="tweet"></div> 
	<div class="social">FOLLOW US<br></div>
	<div class="socialicons"><a href="http://twitter.com/#!/gorilla_editors" title="gorilla_editors on twitter" target="_blank"><img src="/wp-content/themes/gorilla/images/twitter.png"></a>&nbsp;&nbsp;<a href="https://www.facebook.com/pages/Gorilla-Film-Editors/200961099944016" title="Gorilla on Facebook" target="_blank"><img src="/wp-content/themes/gorilla/images/facebook.png"></a></div> 
</div>
<!-- G -->
<?php get_footer(); ?>
